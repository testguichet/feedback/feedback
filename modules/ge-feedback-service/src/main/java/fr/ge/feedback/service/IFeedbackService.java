/**
 *
 */
package fr.ge.feedback.service;

import java.util.List;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.feedback.service.bean.FeedbackBean;

/**
 * The Interface IFeedbackService.
 *
 * @author bsadil
 */
public interface IFeedbackService {

    /**
     * Creates the FeedbackBean.
     *
     * @param entity
     *            the FeedbackBean
     * @return the long
     */
    long create(FeedbackBean entity);

    /**
     * Update the FeedbackBean.
     *
     * @param entity
     *            the FeedbackBean
     * @return the long
     */
    long update(FeedbackBean entity);

    /**
     * delete queue.
     *
     * @param id
     *            of queue
     * @return the long
     */
    long deleteById(Long id);

    /**
     * delete queue.
     *
     * @param string
     *            the string
     * @return the long
     */
    long deleteByPage(String string);

    /**
     * Find by id.
     *
     * @param id
     *            the id
     * @return the feedback bean
     */
    FeedbackBean findById(Long id);

    /**
     * Find by page.
     *
     * @param page
     *            the page
     * @return the list
     */
    List<FeedbackBean> findByPage(String page);

    /**
     * Search for queue messages.
     *
     * @param <R>
     *            the generic type
     * @param query
     *            search parameters
     * @param expectedClass
     *            the expected class
     * @return search result
     */
    <R> SearchResult<R> search(SearchQuery query, Class<R> expectedClass);

}
