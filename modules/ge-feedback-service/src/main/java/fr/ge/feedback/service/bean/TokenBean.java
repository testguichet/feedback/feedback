/**
 *
 */
package fr.ge.feedback.service.bean;

/**
 * The Class TokenBean.
 *
 * @author bsadil
 */
public class TokenBean extends AbstractDatedBean<TokenBean> {

    /** The tocken. */
    private String tocken;

    /** The ip. */
    private String ip;

    /**
     * Instantiates a new token bean.
     */
    public TokenBean() {
        super();
        // Nothing to do
    }

    /**
     * Gets the tocken.
     *
     * @return the tocken
     */
    public String getTocken() {
        return this.tocken;
    }

    /**
     * Sets the tocken.
     *
     * @param tocken
     *            the tocken to set
     */
    public void setTocken(final String tocken) {
        this.tocken = tocken;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp() {
        return this.ip;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the ip to set
     */
    public void setIp(final String ip) {
        this.ip = ip;
    }

}