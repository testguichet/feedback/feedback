/**
 *
 */
package fr.ge.feedback.service.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.feedback.service.bean.FeedbackBean;

/**
 * The Interface IFeedbackMapper.
 *
 * @author bsadil
 */
public interface IFeedbackMapper {

    /**
     * Creates the FeedbackBean.
     *
     * @param entity
     *            the FeedbackBean
     * @return the long
     */
    long create(@Param("feedback") FeedbackBean entity);

    /**
     * Update the FeedbackBean.
     *
     * @param entity
     *            the FeedbackBean
     * @return the long
     */
    long update(@Param("feedback") FeedbackBean entity);

    /**
     * delete queue.
     *
     * @param id
     *            of queue
     * @return the long
     */
    long deleteById(@Param("id") Long id);

    /**
     * delete queue.
     *
     * @param string
     *            the string
     * @return the long
     */
    long deleteByPage(@Param("page") String string);

    /**
     * Find by id.
     *
     * @param id
     *            the id
     * @return the feedback bean
     */
    FeedbackBean findById(@Param("id") Long id);

    /**
     * Find by page.
     *
     * @param page
     *            the page
     * @return the list
     */
    List<FeedbackBean> findByPage(@Param("page") String page);

    /**
     * Search for queue messages.
     *
     * @param filters
     *            filters
     * @param orders
     *            orders
     * @param rowBounds
     *            pagination
     * @return feedback messages list
     */
    List<FeedbackBean> findAll(@Param("filters") Map<String, Object> filters, @Param("orders") List<SearchQueryOrder> orders, RowBounds rowBounds);

    /**
     * Count queue messages.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    long count(@Param("filters") Map<String, Object> filters);
}
