/**
 *
 */
package fr.ge.feedback.service.bean;

/**
 * The Class FeedbackBean.
 *
 * @author bsadil
 */
public class FeedbackBean extends AbstractDatedBean<FeedbackBean> {

    /** the comment of feedback. */
    private String comment;

    /** the page of feedback. */
    private String page;

    /** the score of feedBack. */
    private Long rate;

    /**
     * Instantiates a new feedback bean.
     */
    public FeedbackBean() {
        super();
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * Gets the page.
     *
     * @return the page
     */
    public String getPage() {
        return this.page;
    }

    /**
     * Sets the page.
     *
     * @param page
     *            the page to set
     */
    public void setPage(final String page) {
        this.page = page;
    }

    /**
     * Gets the rate.
     *
     * @return the score
     */
    public Long getRate() {
        return this.rate;
    }

    /**
     * Sets the rate.
     *
     * @param rate
     *            the new rate
     */
    public void setRate(final Long rate) {
        this.rate = rate;
    }

}