package fr.ge.feedback.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.test.AbstractDbTest;
import fr.ge.feedback.service.bean.FeedbackBean;

/**
 * Tests {@link IssueMapper}.
 *
 * @author $Author: aBsibiss $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class FeedbackMapperTest extends AbstractDbTest {

    /** The issue mapper. */
    @Autowired
    private IFeedbackMapper mapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testCreate() {
        final FeedbackBean feedback = new FeedbackBean();
        feedback.setComment("AWAIT");
        feedback.setPage("page1");
        feedback.setCreated(new Date());
        feedback.setUpdated(new Date());
        feedback.setRate(1L);

        this.mapper.create(feedback);
    }

    @Test
    public void testUpdate() {
        final FeedbackBean feedback = new FeedbackBean();

        feedback.setId(100L);
        feedback.setComment("very bad");
        feedback.setRate(1L);
        feedback.setUpdated(new Date());

        this.mapper.update(feedback);

        assertThat(this.mapper.findById(100L), allOf(Arrays.asList( //
                hasProperty("id", equalTo(100L)), //
                hasProperty("comment", equalTo("very bad"))//
        )));
    }

    @Test
    public void testFindById() {
        assertThat(this.mapper.findById(100L), allOf(Arrays.asList( //
                hasProperty("id", equalTo(100L)), //
                hasProperty("comment", equalTo("the best page"))//
        )));
    }

    @Test
    public void testFindByPage() {
        assertThat(this.mapper.findByPage("auth.dev.guichet-entreprises.fr/form-forge/markov/monitoring").get(0), allOf(Arrays.asList( //
                hasProperty("id", equalTo(100L)), //
                hasProperty("comment", equalTo("the best page"))//
        )));
    }

    @Test
    public void deleteById() {
        this.mapper.deleteById(100L);
    }

    @Test
    public void deleteByPage() {
        this.mapper.deleteByPage("auth.dev.guichet-entreprises.fr/form-forge/markov/monitoring");
    }

}
