package fr.ge.feedback.webclient.controller;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.web.search.bean.DatatableSearchQuery;
import fr.ge.common.utils.web.search.bean.DatatableSearchQueryColumn;
import fr.ge.common.utils.web.search.bean.DatatableSearchQueryOrder;
import fr.ge.common.utils.web.search.bean.DatatableSearchResult;
import fr.ge.feedback.ws.v1.bean.ResponseFeedbackBean;

/**
 * The Class AbstractSearchControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(MockitoJUnitRunner.class)
public class AbstractSearchControllerTest {

    /** The controller. */
    @Mock
    private AbstractSearchController<ResponseFeedbackBean> controller;

    /** The order captor. */
    @Captor
    private ArgumentCaptor<List<String>> orderCaptor;

    /** search query captor. */
    @Captor
    private ArgumentCaptor<SearchQuery> searchQueryCaptor;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        reset(this.controller);
        when(this.controller.search()).thenCallRealMethod();
        when(this.controller.searchData(any(), any())).thenCallRealMethod();
    }

    /**
     * Test search init.
     */
    @Test
    public void testSearchInit() {
        when(this.controller.templatePrefix()).thenReturn("feedback");
        final String actual = this.controller.search();

        assertEquals("feedback/search/main", actual);
    }

    /**
     * Test search data empty no order.
     */
    @Test
    public void testSearchDataEmptyNoOrder() {
        final SearchResult<ResponseFeedbackBean> searchResult = new SearchResult.Builder<ResponseFeedbackBean>() //
                .setStartIndex(0).setMaxResults(10) //
                .setTotalResults(0) //
                .build();

        when(this.controller.search(any())).thenReturn(searchResult);

        final DatatableSearchResult<ResponseFeedbackBean> actual = this.controller.searchData(new DatatableSearchQuery().setDraw(1).setStart(0).setLength(10), null);

        assertEquals(1, actual.getDraw());
        assertEquals(0, actual.getData().size());
        assertEquals(0, actual.getRecordsTotal());
        assertEquals(0, actual.getRecordsFiltered());

        verify(this.controller).search(this.searchQueryCaptor.capture());

        assertTrue(this.searchQueryCaptor.getValue().getOrders().isEmpty());
    }

    /**
     * Test search data no order.
     */
    @Test
    public void testSearchDataNoOrder() {
        final SearchResult<ResponseFeedbackBean> searchResult = new SearchResult.Builder<ResponseFeedbackBean>() //
                .setStartIndex(0).setMaxResults(10) //
                .setTotalResults(2) //
                .setContent(new ResponseFeedbackBean(), new ResponseFeedbackBean()) //
                .build();

        when(this.controller.search(any())).thenReturn(searchResult);

        final DatatableSearchResult<ResponseFeedbackBean> actual = this.controller.searchData(new DatatableSearchQuery().setDraw(1).setStart(0).setLength(10), null);

        assertEquals(1, actual.getDraw());
        assertEquals(2, actual.getData().size());
        assertEquals(2, actual.getRecordsTotal());
        assertEquals(2, actual.getRecordsFiltered());

        verify(this.controller).search(this.searchQueryCaptor.capture());

        assertTrue(this.searchQueryCaptor.getValue().getOrders().isEmpty());
    }

    /**
     * Test search data empty.
     */
    @Test
    public void testSearchDataEmpty() {
        final SearchResult<ResponseFeedbackBean> searchResult = new SearchResult.Builder<ResponseFeedbackBean>() //
                .setStartIndex(0).setMaxResults(10) //
                .setTotalResults(0) //
                .build();

        when(this.controller.search(any())).thenReturn(searchResult);

        this.controller.searchData( //
                new DatatableSearchQuery().setDraw(1).setStart(0).setLength(10) //
                        .setColumns(Arrays.asList(new DatatableSearchQueryColumn().setData("col1"))) //
                        .setOrder(Arrays.asList(new DatatableSearchQueryOrder().setColumn(0).setDir("desc"))),
                null //
        );

        verify(this.controller).search(this.searchQueryCaptor.capture());

        final List<SearchQueryOrder> orders = this.searchQueryCaptor.getValue().getOrders();

        assertThat(orders.get(0), allOf(hasProperty("column", equalTo("col1")), hasProperty("order", equalTo("desc"))));
    }

}
