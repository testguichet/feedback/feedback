define([ 'jquery' ], function ($) {

    var Loader = {};

    var _backdrop;
    var _root;
    var _loader;
    var _isShown;

    $(function () {
        _root = $('body');
    });

    Loader.show = function () {
        if (_isShown) {
            return;
        }

        _isShown = true;
        _backdrop = $('<div class="loader-backdrop"></div>').appendTo(_root);

        _backdrop.fadeIn(function () {
            _loader = $('<div class="loader"><object type="image/svg+xml" data="' + baseUrl + '/images/ring.svg" /></div>').appendTo(_root);
        });
    };

    Loader.hide = function () {
        if (!_isShown) {
            return;
        }

        if (_loader) {
            _loader.remove();
            _loader = null;
        }

        if (_backdrop) {
            _backdrop.fadeOut(function () {
                _backdrop.remove();
                _backdrop = null;
            });
        }

        _isShown = false;
    };

    return Loader;

});
