/*!
 * Depends:
 *      lib/func/translate.js
 */
define([ 'lib/func/translate' ], function(translate) {

    return function(value) {
        var patterns = [ '< 1kB', '{0} kB', '{0} MB', '{0} GB', '{0} TB' ];
        var reduce = value;

        if (null == value) {
            return 'unknown';
        }

        var pattern = patterns.find(function(pattern, idx) {
            if (reduce < 1024) {
                return true;
            } else {
                reduce = reduce / 1024;
                return false;
            }
        });

        return pattern ? translate(pattern, Math.ceil(reduce)) : value.toString();
    };

});
