package fr.ge.feedback.webclient.controller.search;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.feedback.webclient.controller.AbstractSearchController;
import fr.ge.feedback.ws.v1.bean.ResponseFeedbackBean;
import fr.ge.feedback.ws.v1.service.IFeedbackPrivateRestService;

/**
 * The Class RecordSearchController.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping("/feedback")
public class FeedbackSearchController extends AbstractSearchController<ResponseFeedbackBean> {

    /** The service. */
    @Autowired
    private IFeedbackPrivateRestService feedbackRestService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {
        return "feedback";
    }

    /**
     * Get search filters.
     *
     * @return the filters
     */
    @Override
    protected List<SearchQueryFilter> getDefaultSearchFilters() {
        return Collections.emptyList();
    }

    /**
     * Edits the.
     *
     * @param model
     *            the model
     * @param id
     *            the id
     * @return the string
     */
    @RequestMapping(value = "/{id}/display", method = RequestMethod.GET)
    public String edit(final Model model, @PathVariable("id") final long id) {
        final ResponseFeedbackBean bean = this.feedbackRestService.get(id);

        model.addAttribute("bean", bean);

        return "feedback/display/main";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<ResponseFeedbackBean> search(final SearchQuery query) {
        return this.feedbackRestService.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders());
    }

}
