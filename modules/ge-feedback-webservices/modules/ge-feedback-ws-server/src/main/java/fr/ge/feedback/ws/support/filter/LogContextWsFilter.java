package fr.ge.feedback.ws.support.filter;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Filter to add information in all the logs. A map of information is added into
 * the logging context.
 *
 * @author Christian Cougourdan
 */
public class LogContextWsFilter extends GenericFilterBean {

    private static final String REGEX_CORRELATION = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private static final String REGEX_USER_ID = "[0-9]{4}-[0-9]{2}-[A-Z]{3}-[A-Z]{3}-[0-9]{2}";

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        try {
            String correlationId = null;
            String userId = null;
            if (request instanceof HttpServletRequest) {
                final HttpServletRequest httpRequest = (HttpServletRequest) request;
                correlationId = Optional.ofNullable(httpRequest.getHeader("X-Correlation-ID")).filter(header -> header.matches(REGEX_CORRELATION)).orElse(null);
                userId = Optional.ofNullable(httpRequest.getHeader("X-User-ID")).filter(header -> header.matches(REGEX_USER_ID)).orElse(null);
            }

            if (null == correlationId) {
                correlationId = UUID.randomUUID().toString();
            }

            MDC.put("correlationId", correlationId);
            MDC.put("userId", userId);
            chain.doFilter(request, response);
        } finally {
            MDC.clear();
        }
    }

}
