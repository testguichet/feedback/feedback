package fr.ge.feedback.ws.util;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Christian Cougourdan
 *
 */
public final class CoreUtil {

    private CoreUtil() {
        // Nothing to do
    }

    /**
     * Search and replace.
     *
     * @param source
     *            source string to search in
     * @param search
     *            regular expression
     * @param replace
     *            replacement functional method
     * @return resulting replacement
     */
    public static String searchAndReplace(final String source, final Pattern search, final Function<Matcher, String> replace) {
        final StringBuffer buffer = new StringBuffer();

        final Matcher matcher = search.matcher(source);
        while (matcher.find()) {
            final String rep = replace.apply(matcher);
            matcher.appendReplacement(buffer, rep);
        }

        matcher.appendTail(buffer);

        return buffer.toString();
    }

}
