/**
 *
 */
package fr.ge.feedback.ws.v1.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.feedback.service.IFeedbackService;
import fr.ge.feedback.service.bean.FeedbackBean;
import fr.ge.feedback.ws.util.CoreUtil;
import fr.ge.feedback.ws.v1.service.IFeedbackRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

/**
 * @author bsadil
 *
 */
@Api("Feedback - Public Rest Services")
@Path("/public/v1/feedback")
public class FeedbackRestServiceImpl implements IFeedbackRestService {

    private static final int MAX_TEXT_SIZE = 254;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackRestServiceImpl.class);

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("[$][{]([^}]+)[}]");

    private String widgetScriptResource;

    @Autowired
    private Properties appProperties;

    @Autowired
    private IFeedbackService feedbackService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response createFeedBack(@ApiParam("User feedback  for specific page ") final String comment, @ApiParam("URI of page ") final String page, @ApiParam("User evaluation") final Long rate) {
        final FeedbackBean feedback = new FeedbackBean();
        feedback.setComment(StringUtils.substring(comment, 0, MAX_TEXT_SIZE));
        feedback.setPage(StringUtils.substring(page, 0, MAX_TEXT_SIZE));
        feedback.setRate(rate);
        feedback.setCreated(new Date());
        feedback.setUpdated(new Date());

        this.feedbackService.create(feedback);

        return Response.ok(feedback.getId()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response updateFeedBack(@ApiParam("User feedback  for specific page ") final String comment, @ApiParam("URI of page ") final String page, @ApiParam("User evaluation") final Long rate,
            @ApiParam("id of feedback") final Long id) {

        final FeedbackBean feedback = new FeedbackBean();
        feedback.setId(id);
        feedback.setComment(StringUtils.substring(comment, 0, MAX_TEXT_SIZE));
        feedback.setPage(StringUtils.substring(page, 0, MAX_TEXT_SIZE));
        feedback.setRate(rate);
        feedback.setUpdated(new Date());

        this.feedbackService.update(feedback);

        return Response.ok(feedback.getId()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response widget() {

        final String scriptResource = this.getWidgetFromCache();
        if (null == scriptResource) {
            return Response.noContent().build();
        } else {
            return Response.ok(scriptResource, "text/javascript").build();
        }
    }

    private String getWidgetFromCache() {
        this.widgetScriptResource = null;
        if (null == this.widgetScriptResource) {
            try (final InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("public/js/widget.js")) {
                final String script = new String(IOUtils.toByteArray(in), StandardCharsets.UTF_8);
                this.widgetScriptResource = CoreUtil.searchAndReplace(script, PLACEHOLDER_PATTERN, m -> {
                    final String key = m.group(1);
                    final String value = this.appProperties.getProperty(key, m.group());
                    return value.replace("$", "\\$");
                });
            } catch (final IOException ex) {
                LOGGER.warn("Unable to read widget script", ex);
            }
        }

        return this.widgetScriptResource;
    }

}
