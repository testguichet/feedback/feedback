/**
 *
 */
package fr.ge.feedback.ws.v1.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class ResponseFeedbackBean.
 *
 * @author bsadil
 */

@XmlRootElement(name = "messageQueue", namespace = "http://v1.ws.feedback.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseFeedbackBean extends AbstractResponseDatedBean<ResponseFeedbackBean> {

    /** the comment of feedback. */
    private String comment;

    /** the page of feedback. */
    private String page;

    /** the score of feedBack. */
    private Long rate;

    /**
     * Instantiates a new response feedback bean.
     */
    public ResponseFeedbackBean() {
        super();
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * Gets the page.
     *
     * @return the page
     */
    public String getPage() {
        return this.page;
    }

    /**
     * Sets the page.
     *
     * @param page
     *            the page to set
     */
    public void setPage(final String page) {
        this.page = page;
    }

    /**
     * Gets the rate.
     *
     * @return the score
     */
    public Long getRate() {
        return this.rate;
    }

    /**
     * Sets the rate.
     *
     * @param rate
     *            the new rate
     */
    public void setRate(final Long rate) {
        this.rate = rate;
    }

}
